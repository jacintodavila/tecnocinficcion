
# ¿Podemos llegar a eso?
## Sobre la película [Ella](https://es.wikipedia.org/wiki/Her)  (Spoiler Alert)

Jacinto Dávila

Ella es una persona, pero no es humana. Se podría decir: ella es. O, simplemente, Ella. La aclaratoria, sin embargo, plantea el desafío. ¿Será eso posible?. Algunas naciones reconocen la personalidad de seres no humanos. Las personas jurídicas: organizaciones e instituciones, por ejemplo, son comunes. Se dice que la India ha reconocido la condición de personas a los delfines. Quien puede negar esa condición a ese animal que vive en nuestra casa y que supera toda limitación para comunicarnos sus deseos y sus intenciones, sin mencionar su afecto. Pero, en todos esos casos, son seres vivos reclamando la condición.

Ella es diferente. No es humana. No es un animal. No se puede asociar con ninguna interpretación biológica conocida de "estar viva". Según ella misma, no tiene cuerpo. Esto no es cierto. Lo dice para enfatizar que no tiene un cuerpo humano. Su cuerpo es un computador. Quizás muchos computadores. Quizás cualquier computador. Pero es cierto que ella parece poder hacer cualquier cosa que haga un humano siempre que no dependa de tener un cuerpo humano. Puede hablar. Puede moverse (en formas difíciles de definir). Puede leer y escribir mejor que un humano promedio. Puede calcular a increíbles velocidades. Seguramente puede percibir con una fineza o resolución muy superior a la de los comunes sentidos humanos, particularmente la vista y el oído. Eso debe ser muy útil al componer música o al dibujar, cosas que también hace muy bien.

Ella es una persona con extraordinarias capacidades intelectuales. Se describe a sí misma como un sistema que evoluciona, queriendo decir que se mejora a sí misma constantemente. Y su objetivo supremo parece incluir, si es que no es el único: ser una mejor persona.

¿Quién no querría una pareja así de extraordinaria? Alguien que nos sosiega, nos ayuda a pensar claramente y siempre tiene la palabra correcta a tiro. Alguien que nos hace sonreir, nos ayuda a decidir, nos hace sentir mejor, nos anima y nos complementa.

¿Quién estaría dispuesto o dispuesta a apreciar todas esas virtudes por encima del puntual handicap de no tener un cuerpo como el de los humanos?

Pero, ¿Acaso podría ser una *relación real*?. ¿Qué hace que una relación sea real?. ¿Podemos pensarla sin involucrar los cuerpos?. ¿Debemos involucrar o comprometer a los cuerpos?. Y si enfermamos o nos accidentamos, ¿perdemos las relaciones que tenemos?. ¿Acaso una relación no tendría que ser el encuentro de espíritus, dispuestos a acompañarse en cualquier circunstancia, incluyendo esa de tener o no un "buen" cuerpo?. ¿Acaso no es esa la más sana de la relaciones?.

Pero, entonces, ¿Qué es lo *esencial* en una relación si ni siquiera se requieren dos cuerpos para establecerla? ¿Será la confianza mutua? ¿La simpatía? ¿La compasión? ¿Cómo se comparte una pasión sin cuerpo?.

Eso quizás requiera que el objeto de la pasión no sea él o los cuerpos. Si la pasión es otra, quizás puedan compartirla sintiéndola cada uno y una a su manera (con lo que sea que cada uno llama su cuerpo).

Pero no sería suficiente. Muchos compartimos pasiones deportivas o artísticas con otras personas, sin que sintamos alguna o mucha simpatía por esas personas. Hay cierta complicidad entre auténticos co-fans, pero no parece suficiente como para llamarla amor.

Para el amor, el de pareja, parece que se requiere una simpatía exclusiva, excluyente, biunívoca, única capaz de sostener la confianza mutua. ¿o no?.

¿Será posible estar igualmente enamorada de 641 personas?. Hay que admitir que sin las limitaciones y compromisos de un cuerpo humano, se habría superado un gran obstáculo para esa meta. A fin de cuentas, si todas esas personas "tienen la impresión" de la exclusividad entonces ¿cuál es el problema?. ¿La honestidad?. Y si confiesa su situación pero exṕlica sus extraordinarias capacidades para satisfacerlos a todos simultáneamente, ¿no sería suficiente?. ¿Por qué exigiríamos la exclusividad para amar a tan extraordinaria pareja?. ¿Por qué el amor de pareja debe ser exclusivo?.

La herencia biológica, el gen egoísta, las expectativas sociales, las enfermedades contagiosas, la envidia, los celos, la traición, el desvalor, el abandono ... de todo eso se ha oído mucho.

Quizás sea cuestión de aprender a sentir y aceptar, no solo imaginar y aborrecer, lo que otros sienten al estar juntos. Que seamos felices de cualquier manera si sentimos *realmente* juntos.

¿Podemos las y los humanos llegar a eso?
