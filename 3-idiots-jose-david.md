# **¿Por qué les dicen idiotas? por José López**


*3 idiots*, producción de la industria **bollywoodense** [[1]], narra la vida universitaria de 
principalmente cuatro jóvenes: *Rancho*, *Farhan*, *Raju* y *Chatur*. Los tres primeros, amigos 
de paso, buscarán desafiar la metodología de aprendizaje que impera en el *Imperial College 
of Engineering* mientras que el último, *Chatur*, refleja esencialmente el método de 
aprendizaje tradicional de esa institución (además de una actitud bastante detestable). Todo 
esto bajo la mirada evaluadora del *Dr. Viru*, director del *ICE*, que no apoya la percepción de 
este mundo que tienen *Rancho* y sus amigos.


*Rancho* posee dos de las características que llevan el hilo de los sucesos; una, el poder 
alcanzar la excelencia académica no haciendo uso de los métodos tradicionales de 
aprendizaje, y por otra parte, servir de orientación en su núcleo de amistad. *Rancho* busca 
entonces, transformar a través de su modo de ver las cosas y los resultados que obtiene, 
lo cual le trae una serie de inconvenientes, como es obvio, con las autoridades, y hasta con sus propios amigos.


*“Conviértete en un buen ingeniero y el éxito te perseguirá a ti”* le dice *Rancho* a *Chatur*, 
luego este último pide encontrarse de nuevo en diez años, mismo día mismo lugar, y así 
ver quién es más exitoso. *Chatur* tiene entonces una mirada del éxito a través de lo 
remunerable, lo que se pueda medir en bienes muebles  e inmuebles, lo alto –
jerárquicamente– que se pueda llegar dentro de una organización, etc. Mientras que 
*Rancho* comulga con lo *holístico*: no basta con medir qué tanto dinero se tenga o qué tan 
importante sea el puesto que se ocupa laboralmente, también se debe estar cerca de lo 
humano, de perseguir lo que se quiere más allá de lo que imponga la norma social. De 
hecho, *Rancho* logra “alcanzar” el bienestar económico, como se puede observar al final de 
la película pero, sin dejar sus valores de lado en la competencia feroz que existe para llegar
ahí.


Tanto a *Farhan* (quien de verdad lo que anhela es convertirse en fotógrafo de vida salvaje 
pero sus padres han puesto todo su esfuerzo en que sea ingeniero) y *Raju* (este, presionado 
por una situación difícil económicamente en su casa, pero convencido de amar la 
ingeniería) se ven guiados, a través de *Rancho* para poder alcanzar sus sueños, sin miedos 
y verdaderamente contentos con lo que hacen.


Lo descrito desde el inicio va entonces dando respuesta a la pregunta *¿Por qué les dicen 
idiotas?* En líneas generales parece no aceptarse en la sociedad que las ciencias aplicadas, 
como la ingeniería, puedan tener matices humanistas, distinto a lo que puede verse en otras 
carreras como las artísticas por ejemplo. Personajes como *Rancho*, que renuncian a la 
competencia, a los métodos tradicionales de enseñanza, que retan al sistema porque siente 
que de una forma u otra los oprime, son vistos sí, como *idiotas*. Basándose en esto, “no 
debe” mezclarse la ciencia con el corazón entonces.


*Rancho*, aparte de ser idiota, se siente cómodo y contento con ello **¿Es el salmón *idiota* 
por nadar contracorriente para poder reproducirse luego de alcanzar su madurez?** [[2]] Seguramente, mientras avanza en la búsqueda de lo que su naturaleza le pide, se esté 
repitiendo constantemente: *Aal Izz Well, Aal Izz Well*.


[1]: https://es.wikipedia.org/wiki/Bollywood

[2]: https://en.wikipedia.org/wiki/Salmon_run