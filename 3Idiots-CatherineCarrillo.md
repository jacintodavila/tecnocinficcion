## Película 3 Idiots
### ¿Por qué les dicen idiotas?


Rancho, Farhan y Raju son tres estudiantes de ingeniería catalogados como idiotas, tanto por el director, como por algunos profesores y compañeros de la Universidad. Ellos simplemente no encajaban en los patrones del sistema tradicional.

A Rancho le apasionaba aprender, pero no de la forma mecánica que le gustaba a sus profesores y a un compañero en particular, llamado Chatur. Es por ello que se vuelve rebelde y es desafiado por el director y por Chatur. Sin embargo, es un genio, estudia cada situación y aplica lo que ha aprendido, nada podía detenerlo.

Farhan no obtenía buenas calificaciones, tal vez porque no se enfocaba o sencillamente no le importaba porque tenía otra pasión que era la fotografía y triunfó siguiéndola.

A Raju, a pesar de gustarle la ingeniería, vivía aferrado a sus miedos porque debía sacar a su familia adelante, era mucho peso sobre sus hombros. No obstante, una vez que dejó ir sus miedos logró conseguir un trabajo, como ingeniero, y ser exitoso.

Respondiendo a la pregunta: ¿Por qué les dicen idiotas? Son idiotas por no ir por lo seguro, por no querer agradarles a los demás, por no seguir las reglas. Cada uno vivió su vida a su manera, siguió su sueño y triunfó en lo que quería hacer.
