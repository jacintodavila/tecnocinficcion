### ¿Por qué son idiotas?

Andrés Aguilar

El sistema educativo actual basado en el modelo educativo de Prusia [1] deja un aprendizaje preventivo donde la pregunta “¿Para qué sirve esto?” la respuesta siempre será “Por si algún día lo llegas a necesitar”. El conocimiento está cambiando permanentemente, sin embargo, el sistema educativo no ha cambiado tan rápidamente como el resto de la sociedad.

Un objetivo es aquello que es medible, cuantificable y observable [2] de ahí la necesidad de buscar una regla que pueda medir los objetivos, esto lo conocemos como las calificaciones, incluso cuando los exámenes no son capaces de puntuar el verdadero potencial de una persona, es el sistema utilizado para definir la excelencia académica y crear un ranking que solo provoca tensión, presión y mucha competición (“Todo el mundo habla de paz, pero nadie educa para la paz. La gente educa para la competencia, y la competencia es el principio de cualquier guerra” [3]).

El aprendizaje solo puede estar fundado por el interés, la voluntad y la curiosidad, es mucho más que analizar y relacionar conceptos. Aprender implica un profundo proceso donde se crean relaciones entre la persona y su entorno. Hoy el libre acceso a la información logra que el conocimiento esté al alcance de todos, de forma abierta y plural y además se actualiza constantemente. 

Las personas insisten en estructurar, limitar, condicionar y ordenar el aprendizaje consideran idiotas a las personas que son rebeldes al sistema, que creen que es injusto, quieren aprender por aprender, mantener sus principios y cuestionan todo.

Rancho, Farhan y Raju nos muestran como decisiones que no tomamos por voluntad propia puede que te lleve a un salario (tampoco lo asegura), pero, definitivamente, esa decisión puede traer mucha insatisfacción a nivel personal y además ser un profesional mediocre. Debemos escoger por nosotros mismos lo que nos gusta y dedicarnos a eso. Ingeniárselas en situaciones que no se han previsto (que no se aprende en el sistema educativo).

Si ser idiota es ser como los protagonistas de esta historia ¡Viva la idiotez!



Referencias:

[1] https://es.wikipedia.org/wiki/Sistema_educativo_de_Alemania

[2] http://www.definicionabc.com/general/objetivo.php

[3] https://muhimu.es/educacion/pablo-lipnisky/
